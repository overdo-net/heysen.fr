![Bannière heysen.fr](https://i.imgur.com/FdEsFq7.png)

# 🚀 Introduction

Heysen.fr est mon site web portfolio personnel, hébergé à cette adresse : [https://heysen.fr](https://heysen.fr).
J'y partage mon univers et mes expériences en tant que développeur et designer à travers quelques projets et mes articles de blog. 
(C'est aussi  mon un très bon terrain de jeu pour en apprendre toujours plus 👀)

## 👨🏻‍💻 Technologies utilisées

- Framework Javascript: [Next.js](https://nextjs.org/)
- Framework CSS: [Chakra](https://chakra-ui.com/)
- Syntaxe de code (blog): [Prism.js](https://prismjs.com/)
- Data en temps réel: [Google Firebase](https://firebase.google.com/)
- Articles (blog): [next-mdx-remote](https://github.com/hashicorp/next-mdx-remote)
- Ce que j'écoute en temps réel: [API Spotify ](https://developer.spotify.com/documentation/web-api/)
- Déploiement: [Vercel](https://vercel.com/)


## 📝 MIT License

[![FOSSA Status](https://app.fossa.com/api/projects/git%2Bgithub.com%2Fflorianheysen%2Fheysen.fr.svg?type=large)](https://app.fossa.com/projects/git%2Bgithub.com%2Fflorianheysen%2Fheysen.fr?ref=badge_large)
